package com.example.lab31.pages;

import com.example.lab31.tests.MainPageTest;
import com.example.lab31.util.Configuration;
import com.example.lab31.util.Drivers;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static com.example.lab31.util.Util.click;
import static com.example.lab31.util.Util.sleep;
import static org.openqa.selenium.support.ui.ExpectedConditions.*;

// page_url = https://spb.hh.ru/account/login?backurl=%2F&hhtmFrom=main
public class LoginPage {

    WebDriver driver;
    Drivers.Type type;

    @FindBy(xpath = "//button[@data-qa='expand-login-by-password']")
    private WebElement signInWithPassBtn;

    @FindBy(xpath = "//input[@inputmode='email']")
    private WebElement loginField;

    @FindBy(xpath = "//input[@type='password']")
    private WebElement passField;

    @FindBy(xpath = "//button[@data-qa='account-login-submit']")
    private WebElement signInBtn;

    @FindBy(xpath = "a[data-qa='login']")
    private WebElement loginBtn;

    @FindBy(xpath = "/html/body/div[5]/div/div[3]/div[1]/div/div/div/div/div[3]/div/div[1]/div[1]/div/div[2]/div/div/div[1]")
    private WebElement userNameTitle;


    public LoginPage(WebDriver driver, Drivers.Type type) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
        this.type = type;
    }

    public void signIn(String login, String pass) {
        signInWithPassBtn.click();

        sleep(driver, 3).until(visibilityOf(loginField)).sendKeys(login);
        passField.sendKeys(pass);

        signInBtn.click();

        sleep(driver, 3).until(invisibilityOf(loginBtn));
    }

    public String getUserName() {
        driver.get(Configuration.get("settingspage"));

        return userNameTitle.getText();
    }
}
