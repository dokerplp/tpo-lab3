package com.example.lab31.pages;

import com.example.lab31.util.Drivers;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

// page_url = https://spb.hh.ru/
public class MainPage {
    public WebDriver driver;

    public Drivers.Type type;

    @FindBy(xpath = "//a[@data-qa='login']")
    public WebElement signInBtn;

    @FindBy(xpath = "//input[@data-qa='search-input']")
    public WebElement searchJobField;

    @FindBy(xpath = "//span[@class='supernova-search-submit-text']")
    public WebElement searchJobBtn;

    public MainPage(WebDriver driver, Drivers.Type type) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
        this.type = type;
    }

    public void clickSignInBtn() {
        signInBtn.click();
    }

    public void searchJob(String job) {
        searchJobField.sendKeys(job);
        searchJobBtn.click();
    }
}
