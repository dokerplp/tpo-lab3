package com.example.lab31.pages;

import com.example.lab31.util.Configuration;
import com.example.lab31.util.Drivers;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import static com.example.lab31.util.Util.*;
import static org.openqa.selenium.support.ui.ExpectedConditions.*;

// page_url = https://spb.hh.ru/applicant/resumes
public class ResumePage {

    WebDriver driver;
    Drivers.Type type;

    @FindBy(xpath = "//span[@class='b-marker']")
    public WebElement myResumeBtn;

    @FindBy(xpath = "//a[@data-qa='resume-block-position-edit']")
    public WebElement mainInfoBtn;

    public static class MainInfo {
        @FindBy(xpath = "/html/body/div[5]/div/div[3]/div[1]/div/div[2]/div/div/div[4]/div[1]/div/div/div[1]/div[1]/div/div[1]/div[1]/h2/span/span")
        public WebElement jobTitle;

        @FindBy(xpath = "//span[@class='resume-block__salary']")
        public WebElement moneyTitle;

        @FindBy(xpath = "/html/body/div[5]/div/div[3]/div[1]/div/div/form/div[2]/div[1]/div[2]/div/fieldset/input")
        public WebElement jobField;

        @FindBy(xpath = "/html/body/div[5]/div/div[3]/div[1]/div/div/form/div[4]/div[2]/div[1]/fieldset/input")
        public WebElement moneyField;

        @FindBy(xpath = "/html/body/div[5]/div/div[3]/div[1]/div/div/form/div[4]/div[2]/div[1]/div[1]/select")
        public WebElement moneyCurrencySelect;

        @FindBy(xpath = "/html/body/div[5]/div/div[3]/div[1]/div/div/form/div[8]/div[2]/button/span")
        public WebElement saveBtn;

        public MainInfo(WebDriver driver) {
            PageFactory.initElements(driver, this);
        }
    }

    public MainInfo mainInfo;


    @FindBy(xpath = "//a[@data-qa='resume-block-key-skills-edit']")
    public WebElement skillsBtn;

    public static class Skills {

        @FindBy(xpath = "/html/body/div[5]/div/div[3]/div[1]/div/div/form/div[1]/div/div/div/div/div[2]/div[1]/fieldset/input")
        public WebElement skillsField;

        @FindBy(xpath = "//div[@class='bloko-control-group__minor']")
        public WebElement saveSkillBtn;

        @FindBy(xpath = "//button[@data-qa='resume-submit']")
        public WebElement saveBtn;

        @FindBy(xpath = "/html/body/div[5]/div/div[3]/div[1]/div/div[2]/div/div/div[4]/div[1]/div/div/div[3]/div[2]/div/div/div/div/div[2]/span")
        public WebElement skillTitle;

        @FindBy(xpath = "/html/body/div[5]/div/div[3]/div[1]/div/div/form/div[1]/div/div/div/div/div[1]/div/div[2]/button")
        public WebElement delSkillBtn;

        public Skills(WebDriver driver) {
            PageFactory.initElements(driver, this);
        }
    }

    public Skills skills;


    @FindBy(xpath = "//a[@data-qa='resume-block-education-edit']")
    public WebElement educationBtn;

    public static class Education {
        @FindBy(xpath = "/html/body/div[5]/div/div[3]/div[1]/div/div/form/div[1]/div[2]/div[1]/div[1]/div[2]/div/div/fieldset/input")
        public WebElement institutionField;

        @FindBy(xpath = "/html/body/div[5]/div/div[3]/div[1]/div/div/form/div[1]/div[2]/div[1]/div[2]/div[2]/div/div/div/fieldset/input")
        public WebElement yearField;

        @FindBy(xpath = "/html/body/div[5]/div/div[3]/div[1]/div/div/form/div[3]/div[2]/button/span")
        public WebElement saveBtn;

        @FindBy(xpath = "/html/body/div[5]/div/div[3]/div[1]/div/div[2]/div/div/div[4]/div[1]/div/div/div[4]/div[2]/div/div/div/div[2]/div/div/span")
        public WebElement sharagaTitle;

        @FindBy(xpath = "//div[@class='bloko-column bloko-column_xs-4 bloko-column_s-2 bloko-column_m-2 bloko-column_l-2']")
        public WebElement yearTitle;

        @FindBy(xpath = "/html/body/div[5]/div/div[3]/div[1]/div/div[2]/div/div/div[4]/div[1]/div/div/div[4]/div[1]/div/div/div[1]/h2/span")
        public WebElement eduTitle;

        public Education(WebDriver driver) {
            PageFactory.initElements(driver, this);
        }
    }

    public Education education;

    @FindBy(xpath = "//a[@data-qa='resume-block-language-edit']")
    public WebElement languagesBtn;

    public static class Languages {
        @FindBy(xpath = "//button[@data-qa='resume-add-language']")
        public WebElement addLanguageBtn;

        @FindBy(xpath = "/html/body/div[5]/div/div[3]/div[1]/div/div/form/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div/div/div[1]/div/div/div")
        public WebElement languageTypeBtn;

        @FindBy(xpath = "//input[@data-qa='bloko-custom-select-search-input']")
        public WebElement langSearchField;

        @FindBy(xpath = "/html/body/div[13]/div/div[2]/div[2]/div/span")
        public WebElement firstFoundLangDiv;

        @FindBy(xpath = "/html/body/div[5]/div/div[3]/div[1]/div/div/form/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div/div/div[2]/div/select")
        public WebElement languageLvlSelect;

        @FindBy(xpath = "/html/body/div[5]/div/div[3]/div[1]/div/div/form/div[3]/div[2]/button/span")
        public WebElement saveBtn;

        @FindBy(xpath = "/html/body/div[5]/div/div[3]/div[1]/div/div[2]/div/div/div[4]/div[1]/div/div/div[5]/div[2]/div/div/div/div[2]/span/p")
        public WebElement langTitle;

        @FindBy(xpath = "/html/body/div[5]/div/div[3]/div[1]/div/div/form/div[1]/div[3]/div[2]/div[1]/div[1]/div[2]/div/span/button")
        public WebElement delLangBtn;

        public Languages(WebDriver driver) {
            PageFactory.initElements(driver, this);
        }
    }

    public Languages languages;


    public ResumePage(WebDriver driver, Drivers.Type type) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
        this.type = type;

        mainInfo = new MainInfo(driver);
        skills = new Skills(driver);
        education = new Education(driver);
        languages = new Languages(driver);
    }

    public void openResume() {
        driver.get(Configuration.get("resumepage"));

        myResumeBtn.click();
    }

    public void fillMainInfo(String job, String money, String currency) {
        mainInfoBtn.click();


        clear(mainInfo.jobField);
        mainInfo.jobField.sendKeys(job);

        clear(mainInfo.moneyField);
        mainInfo.moneyField.sendKeys(money);


        Select select = new Select(mainInfo.moneyCurrencySelect);
        select.selectByVisibleText(currency);

        mainInfo.saveBtn.click();
    }

    public void addSkill(String skill) {
        skillsBtn.click();

        skills.skillsField.sendKeys(skill);
        skills.saveSkillBtn.click();

        skills.saveBtn.click();
    }

    public void delLastSkill() {
        skillsBtn.click();

        skills.delSkillBtn.click();

        skills.saveBtn.click();
    }

    public void addEdu(String institution, String year) {
        educationBtn.click();

        education.institutionField.sendKeys(institution);
        education.yearField.sendKeys(year);

        education.saveBtn.click();
    }

    public void clearEdu() {
        educationBtn.click();

        clear(education.institutionField);
        clear(education.yearField);

        education.saveBtn.click();
    }

    public void addLang(String lang, int lvl) {
        sleep(driver, 3).until(visibilityOf(languagesBtn)).click();

        sleep(driver, 3).until(visibilityOf(languages.addLanguageBtn)).click();
        sleep(driver, 3).until(visibilityOf(languages.languageTypeBtn)).click();
        sleep(driver, 3).until(visibilityOf(languages.langSearchField)).sendKeys(lang);
        sleep(driver, 3).until(visibilityOf(languages.firstFoundLangDiv)).click();

        new Select(languages.languageLvlSelect).selectByIndex(lvl);

        languages.saveBtn.click();
    }

    public void delLang() {
        languagesBtn.click();

        languages.delLangBtn.click();

        languages.saveBtn.click();
    }
}