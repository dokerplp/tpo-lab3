package com.example.lab31.pages;

import com.example.lab31.util.Drivers;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static com.example.lab31.util.Util.click;
import static com.example.lab31.util.Util.sleep;
import static org.openqa.selenium.support.ui.ExpectedConditions.textToBePresentInElement;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

// page_url = https://spb.hh.ru/search/vacancy?text=java&area=2
public class VacancyPage {

    public WebDriver driver;
    public Drivers.Type type;

    @FindBy(xpath = "/html/body/div[5]/div/div[3]/div[1]/div/div[2]/div[2]/div[1]/div[1]/div/div/aside/div[7]/fieldset/div[2]/li[5]/label/span/span[1]")
    public WebElement salary225000Btn;

    @FindBy(xpath = "/html/body/div[5]/div/div[3]/div[1]/div/div[2]/div[2]/div[1]/div[1]/div/div/aside/div[9]/fieldset/div[2]/div/button")
    public WebElement allRegionsBtn;

    @FindBy(xpath = "/html/body/div[5]/div/div[3]/div[1]/div/div[2]/div[2]/div[1]/div[1]/div/div/aside/div[9]/fieldset/div[2]/div[1]/fieldset/input")
    public WebElement regionField;

    @FindBy(xpath = "/html/body/div[5]/div/div[3]/div[1]/div/div[2]/div[2]/div[1]/div[1]/div/div/aside/div[9]/fieldset/div[2]/ul/li/label/span/span")
    public WebElement moscowBtn;

    @FindBy(xpath = "/html/body/div[5]/div/div[3]/div[1]/div/div[2]/div[2]/div[2]/main/div[1]/div[2]/div/div[1]/div/div[3]/span")
    public WebElement salaryValue;

    @FindBy(xpath = "/html/body/div[5]/div/div[3]/div[1]/div/div[2]/div[2]/div[2]/main/div[1]/div[2]/div/div[1]/div/div[4]/div/div[2]")
    public WebElement regionValue;

    public VacancyPage(WebDriver driver, Drivers.Type type) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
        this.type = type;
    }

    public void setFilters() {

        salary225000Btn.click();

        click(driver, allRegionsBtn);

        sleep(driver, 3).until(visibilityOf(regionField)).sendKeys("Москва");
        sleep(driver, 3).until(textToBePresentInElement(moscowBtn, "Москва"));

        click(driver, moscowBtn);

        sleep(driver, 3).until(textToBePresentInElement(regionValue, "Москва"));
    }
}