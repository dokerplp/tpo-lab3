package com.example.lab31.tests;

import com.example.lab31.util.Configuration;
import com.example.lab31.util.Drivers;
import com.example.lab31.pages.LoginPage;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LoginPageTest {
    private WebDriver driver;
    private LoginPage loginPage;

    static Stream<Arguments> drivers() {
        return Drivers.drivers.keySet().stream()
                .map(Arguments::of);
    }

    public void setUp(Drivers.Type type) {
        driver = Drivers.drivers.get(type).get();
        loginPage = new LoginPage(driver, type);

        driver.get(Configuration.get("loginpage"));
    }

    @AfterEach
    public void tearDown() {
        driver.quit();
    }


    @ParameterizedTest
    @MethodSource("drivers")
    public void loginTest(Drivers.Type type) {
        setUp(type);

        loginPage.signIn(Configuration.get("login"), Configuration.get("pass"));

        assertEquals("test test", loginPage.getUserName());
    }
}
