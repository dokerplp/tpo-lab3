package com.example.lab31.tests;

import com.example.lab31.util.Configuration;
import com.example.lab31.util.Drivers;
import com.example.lab31.pages.MainPage;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.openqa.selenium.WebDriver;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MainPageTest {

    public final static String LOGIN_TITLE = "Вход в личный кабинет";
    public final static String SEARCH_JOB_TITLE = "Работа в Санкт-Петербурге, поиск персонала и публикация вакансий - spb.hh.ru";
    private WebDriver driver;
    private MainPage mainPage;

    static Stream<Arguments> drivers() {
        return Drivers.drivers.keySet().stream()
                .map(Arguments::of);
    }

    public void setUp(Drivers.Type type) {
        driver = Drivers.drivers.get(type).get();
        mainPage = new MainPage(driver, type);

        driver.get(Configuration.get("mainpage"));
    }

    @AfterEach
    public void tearDown() {
        driver.quit();
    }

    @ParameterizedTest
    @MethodSource("drivers")
    public void loginTest(Drivers.Type type) {
        setUp(type);

        mainPage.clickSignInBtn();
        assertEquals(LOGIN_TITLE, driver.getTitle());
    }

    @ParameterizedTest
    @MethodSource("drivers")
    public void searchJobTest(Drivers.Type type) {
        setUp(type);

        mainPage.searchJob("java");
        assertEquals(SEARCH_JOB_TITLE, driver.getTitle());
    }
}
