package com.example.lab31.tests;

import com.example.lab31.pages.LoginPage;
import com.example.lab31.pages.MainPage;
import com.example.lab31.pages.ResumePage;
import com.example.lab31.util.Configuration;
import com.example.lab31.util.Drivers;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import static com.example.lab31.util.Util.sleep;
import static org.junit.jupiter.api.Assertions.*;
import static org.openqa.selenium.support.ui.ExpectedConditions.*;

public class ResumePageTest {

    private WebDriver driver;
    private ResumePage resumePage;
    private LoginPage loginPage;

    static Stream<Arguments> drivers() {
        return Drivers.drivers.keySet().stream()
                .map(Arguments::of);
    }


    public void setUp(Drivers.Type type) {
        driver = Drivers.drivers.get(type).get();
        resumePage = new ResumePage(driver, type);
        loginPage = new LoginPage(driver, type);

        driver.get(Configuration.get("loginpage"));
        loginPage.signIn(Configuration.get("login"), Configuration.get("pass"));

        driver.get(Configuration.get("resumepage"));
    }

    @AfterEach
    public void tearDown() {
        driver.quit();
    }

    @ParameterizedTest
    @MethodSource("drivers")
    public void mainInfoTest(Drivers.Type type) {
        setUp(type);

        resumePage.openResume();
        resumePage.fillMainInfo("Таксист", "100000", "руб.");

        assertEquals("Таксист", resumePage.mainInfo.jobTitle.getText());
        assertEquals("100 000 руб. на руки", resumePage.mainInfo.moneyTitle.getText());

        resumePage.openResume();
        resumePage.fillMainInfo("Курьер", "300", "USD");

        String job = sleep(driver, 3).until(visibilityOf(resumePage.mainInfo.jobTitle)).getText();
        String money = sleep(driver, 3).until(visibilityOf(resumePage.mainInfo.moneyTitle)).getText();

        assertEquals("Курьер", job);
        assertEquals("300 USD на руки", money);
    }

    @ParameterizedTest
    @MethodSource("drivers")
    public void skillTest(Drivers.Type type) {
        setUp(type);

        resumePage.openResume();
        resumePage.addSkill("aboba");

        var skillTitle = sleep(driver, 3).until(visibilityOf(resumePage.skills.skillTitle)).getText();
        assertEquals("aboba", skillTitle);

        resumePage.delLastSkill();

        assertThrows(NoSuchElementException.class, () -> resumePage.skills.skillTitle.click());
    }

    @ParameterizedTest
    @MethodSource("drivers")
    public void eduTest(Drivers.Type type) {
        setUp(type);

        resumePage.openResume();
        resumePage.addEdu("Шарага", "2025");

        assertEquals("Шарага", resumePage.education.sharagaTitle.getText());
        assertEquals("2025", resumePage.education.yearTitle.getText());

        resumePage.openResume();
        resumePage.clearEdu();

        assertEquals("Образование", resumePage.education.eduTitle.getText());

    }

    private final Map<Integer, String> langLvls = Map.of(
            0, "A1 — Начальный",
            1, "A2 — Элементарный",
            2, "B1 — Средний",
            3, "B2 — Средне-продвинутый",
            4, "C1 — Продвинутый",
            5, "C2 — В совершенстве"
    );

    @ParameterizedTest
    @MethodSource("drivers")
    public void langTest(Drivers.Type type) {

        setUp(type);
        resumePage.openResume();

        resumePage.addLang("Немецкий", 1);

        assertEquals("Немецкий — " + langLvls.get(1), resumePage.languages.langTitle.getText());

        resumePage.delLang();

        assertThrows(NoSuchElementException.class, () -> resumePage.languages.langTitle.getText());
    }

}
