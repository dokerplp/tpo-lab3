package com.example.lab31.tests;

import com.example.lab31.util.Configuration;
import com.example.lab31.util.Drivers;
import com.example.lab31.pages.VacancyPage;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.openqa.selenium.WebDriver;

import java.util.stream.Stream;

import static com.example.lab31.util.Util.sleep;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class VacancyPageTest {

    private WebDriver driver;
    private VacancyPage vacancyPage;

    static Stream<Arguments> drivers() {
        return Drivers.drivers.keySet().stream()
                .map(Arguments::of);
    }

    public void setUp(Drivers.Type type) {
        driver = Drivers.drivers.get(type).get();
        vacancyPage = new VacancyPage(driver, type);

        driver.get(Configuration.get("vacancypage"));
    }

    @AfterEach
    public void tearDown() {
        driver.quit();
    }


    @ParameterizedTest
    @MethodSource("drivers")
    public void filterTest(Drivers.Type type) {
        setUp(type);
        vacancyPage.setFilters();

        String regionText = sleep(driver, 3).until(visibilityOf(vacancyPage.regionValue)).getText();
        String salaryText = sleep(driver, 3).until(visibilityOf(vacancyPage.salaryValue)).getText();

        String salary = salaryText.replaceAll("[^\\d–]", "").split("–")[0];
        long sal = Long.parseLong(salary);
        assertTrue(sal >= 225000 && salaryText.contains("руб") || sal >= 4000 && salaryText.contains("USD"));
        assertEquals("Москва", regionText);
    }
}
