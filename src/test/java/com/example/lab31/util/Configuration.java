package com.example.lab31.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Configuration {
    private final static Properties PROPERTIES;

    static {
        try (
                InputStream is = new FileInputStream("src/test/resources/conf.properties")
        ) {
            PROPERTIES = new Properties();
            PROPERTIES.load(is);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static String get(String name) {
        return PROPERTIES.getProperty(name);
    }

}
