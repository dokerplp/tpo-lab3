package com.example.lab31.util;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

public class Drivers {
    public static final Map<Type, Supplier<WebDriver>> drivers = new HashMap<>();

    static {
        drivers.put(Type.CHROME, Drivers::chrome);
        drivers.put(Type.FIRE_FOX, Drivers::fireFox);
    }

    private static WebDriver chrome() {
        System.setProperty("webdriver.chrome.driver", Configuration.get("chromedriver"));

        ChromeOptions opt = new ChromeOptions();
        opt.setBinary(Configuration.get("chromebinary"));

        WebDriver driver = new ChromeDriver(opt);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));

        return driver;
    }

    private static WebDriver fireFox() {
        System.setProperty("webdriver.gecko.driver", Configuration.get("firefoxdriver"));

        WebDriver driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));

        return driver;
    }

    public enum Type {
        CHROME, FIRE_FOX
    }
}
