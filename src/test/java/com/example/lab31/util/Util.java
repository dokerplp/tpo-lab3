package com.example.lab31.util;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class Util {
    public static WebDriverWait sleep(WebDriver driver, long seconds) {
        return new WebDriverWait(driver, Duration.ofSeconds(seconds));
    }

    public static void click(WebDriver driver, WebElement element) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", element);
    }

    public static void fill(WebDriver driver, WebElement element, String text) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].value='" + text + "';", element);
    }

    public static void clear(WebElement element) {
        element.sendKeys(Keys.chord(Keys.COMMAND,"a"));
        element.sendKeys(Keys.DELETE);
    }

}
